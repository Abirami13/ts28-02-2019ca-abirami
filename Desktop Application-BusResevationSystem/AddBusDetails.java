package bus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class AddBusDetails {

	public AddBusDetails(){
		JFrame frame=new JFrame("Add Bus Details");
		 frame.setSize(400,500);
		 
		 JLabel routenum=new JLabel("ROUTE NUMBER");
		 routenum.setBounds(23,50,100,30);
		 frame.add(routenum);
		  
		 JTextField numField=new JTextField();
		 numField.setBounds(150,50,100,30);
		 frame.add(numField);
		 
		 JLabel busname=new JLabel("BUS NAME");
		 busname.setBounds(23,100,100,30);
		 frame.add(busname);
		  
		 JTextField busnamefield=new JTextField();
		 busnamefield.setBounds(150,100,100,30);
		 frame.add(busnamefield);
		 
		 
		 
		 JLabel fare=new JLabel("FARE STAGES");
		 fare.setBounds(23,150,100,30);
		 frame.add(fare);
		 
		 JTextField fareField=new JTextField();
		 fareField.setBounds(150,150,100,30);
		 frame.add(fareField);
		  
		 JLabel startpoint=new JLabel("SOURCE");
		 startpoint.setBounds(23,200,100,30);
		 frame.add(startpoint);
		 
		 JTextField city_nameField=new JTextField();
		 city_nameField.setBounds(150,200,100,30);
		 frame.add(city_nameField);
		    
		 
		 JLabel starttime=new JLabel("START TIME");
		 starttime.setBounds(23,250,100,30);
		 frame.add(starttime);
		 
		 JTextField time_idField=new JTextField();
		 time_idField.setBounds(150,250,100,30);
		 frame.add(time_idField);
		 
		 JLabel endpoint=new JLabel("DESTINATION");
		 endpoint.setBounds(23,300,100,30);
		 frame.add(endpoint);
		 
		 JTextField city_nameField2=new JTextField();
		 city_nameField2.setBounds(150,300,100,30);
		 frame.add(city_nameField2);
		
		 JLabel droptime=new JLabel("DROP TIME");
		 droptime.setBounds(23,350,100,30);
		 frame.add(droptime);
		 
		 JTextField time_idField2=new JTextField();
		 time_idField2.setBounds(150,350,100,30);
		 frame.add(time_idField2);
		 
		 JButton save=new JButton("SAVE");
		 save.setBounds(30,400,100,30);
		 
		 	 frame.add(save);
		 	 
		 	 
		 	 
		 	save.addActionListener(new ActionListener()
			 {  
		     public void actionPerformed(ActionEvent e)
		     { 	
		    	 String routenumber=numField.getText();
		    	 String fares=fareField.getText();
		    	 String source=city_nameField.getText();
		    	 String startingtime=time_idField.getText();
		    	 String destination=city_nameField2.getText();
		    	 String dropingtime=time_idField2.getText();
		    	 if(routenumber.isEmpty()||fares.isEmpty()||source.isEmpty()||startingtime.isEmpty()||destination.isEmpty()||dropingtime.isEmpty()) 
		    	 {
		    		 JOptionPane.showMessageDialog(frame," Enter All The Details ");
		    			
		    	 }
				
				try
				{  
					     Class.forName("oracle.jdbc.driver.OracleDriver");  
						   
						 Connection con=DriverManager.getConnection
						 ("jdbc:oracle:thin:@localhost:1521:xe","system","kalajayapal");  
						   
						 PreparedStatement stmt=con.prepareStatement("insert into busdetails values(?,?,?,?,?,?,?)");  
						 
						 			 
						 int routenum=Integer.parseInt(numField.getText());
						 stmt.setInt(1,routenum); 
						 
						 stmt.setString(2,busnamefield.getText());
						 
						 int fare=Integer.parseInt(fareField.getText());
						 stmt.setInt(3,fare); 
						 
						 stmt.setString(4,city_nameField.getText());
					 
						 
						 int starttime=Integer.parseInt(time_idField.getText());
					 stmt.setInt(5,starttime);
							
						 stmt.setString(6,city_nameField2.getText());
						 
						 int droptime=Integer.parseInt(time_idField2.getText());					    
						 stmt.setInt(7,droptime);
					     
						 int i=stmt.executeUpdate();  
						 JOptionPane.showMessageDialog(frame,i+"record inserted"); 
						 System.out.println(i+" record inserted");    
						 con.close();  
						   
				} 
				catch(Exception e1)
				{ 
			      System.out.println(e1);  
						   
				}}      
			     });
		 	 
		 	JButton goBack=new JButton("BACK");
			goBack.setBounds(190,400,100,30);
			goBack.addActionListener(new ActionListener()
			{  
				 public void actionPerformed(ActionEvent e)
				 {  
					 AdminHomePage goback1=new AdminHomePage();
					 frame.dispose();       
			     }  
			});  
			 frame.add(goBack);
		 	
		  
		  

			 frame.setLayout(null);
			 frame.setVisible(true);
}
	}


