package bus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.DriverManager;




public class AgentRegistration {

	public AgentRegistration() {
		JFrame frame=new JFrame("Create a Account");
		 frame.setSize(400,500);
		 
		 JLabel fullname=new JLabel("FULL NAME");
		 fullname.setBounds(23,50,100,30);
		 frame.add(fullname);
		  
		 JTextField nameField=new JTextField();
		 nameField.setBounds(150,50,100,30);
		 frame.add(nameField);
		 
		 JLabel mail=new JLabel("E-MAIL");
		 mail.setBounds(23,100,100,30);
		 frame.add(mail);
		 
		 JTextField mailField=new JTextField();
		 mailField.setBounds(150,100,100,30);
		 frame.add(mailField);
		  
		 JLabel num=new JLabel("MOBILE NUMBER");
		 num.setBounds(23,150,100,30);
		 frame.add(num);
		 
		 JTextField numField=new JTextField();
		 numField.setBounds(150,150,100,30);
		 frame.add(numField);
		 
		 JLabel password=new JLabel("PASSWORD");
		 password.setBounds(23,190,100,30);
		 frame.add(password);
		 
		 JPasswordField pwdField=new JPasswordField();
		 pwdField.setBounds(150,190,100,30);
		 frame.add(pwdField);
		 
		 JLabel gender=new JLabel("GENDER");
		 gender.setBounds(23,240,100,30);
		 frame.add(gender);
		 
		 
		 JRadioButton r1=new JRadioButton("MALE");    
		 JRadioButton r2=new JRadioButton("FEMALE");    
		 r1.setBounds(150,240,100,30);    
		 r2.setBounds(150,280,100,30);    
		 ButtonGroup bg=new ButtonGroup();    
		 bg.add(r1);
		 bg.add(r2);    
		 frame.add(r1);
		 frame.add(r2); 
		 
		 JButton register=new JButton("REGISTER");
		 register.setBounds(50,340,100,30);
		 frame.add(register);
		 


				register.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					
					
						String username1=nameField.getText();
						String password1=pwdField.getText();
						String email=mailField.getText();
						String phNumber=numField.getText();
						if(nameField.getText().length() == 0 || pwdField.getText().length()== 0||mailField.getText().length() == 0||numField.getText().length() == 0)      //check condition null
						{
						JOptionPane.showMessageDialog(null,"Please enter valid details");
						}
						else if(numField.getText().length()<10||numField.getText().length()>10) {
							JOptionPane.showMessageDialog(null,"phonenumber must be digits and the length should be 10");	
							
						}
						
						
						else if(phNumber.isEmpty())
						{
							JOptionPane.showMessageDialog(null, "invalid Phone Number" );   
							
						}
						else {
							
							try {
								Class.forName("oracle.jdbc.driver.OracleDriver");  
								   
								 Connection con=DriverManager.getConnection
								 ("jdbc:oracle:thin:@localhost:1521:xe","system","kalajayapal");  
								 
						
								PreparedStatement st = con.prepareStatement("Insert into agentdetails(username,passwords,phonenumber,emailid)values(?,?,?,?)");
								System.out.println(st);
								st.setString(1, nameField.getText());
								st.setString(2, pwdField.getText());
								st.setString(3, numField.getText());
								st.setString(4, mailField.getText());
								st.executeUpdate();
								JOptionPane.showMessageDialog(null, "registered Successfully");
								st.close();
								con.close();
								//dispose();
							}
							catch(Exception e1){ 
								System.out.println(e1.getMessage()); 
								} 
				}
						
						
						nameField.setText("");
						mailField.setText("");
						numField.setText("");
						pwdField.setText("");
			}
		});
				JButton goBack=new JButton("BACK");
				goBack.setBounds(190,340,100,30);
				goBack.addActionListener(new ActionListener()
				{  
					 public void actionPerformed(ActionEvent e)
					 {  
						 AgentLogin goback1=new AgentLogin();
						 frame.dispose();       
				     }  
				});  
				 frame.add(goBack);
				
				frame.setLayout(null);
				 frame.setVisible(true);
		
			
	}	
	}



