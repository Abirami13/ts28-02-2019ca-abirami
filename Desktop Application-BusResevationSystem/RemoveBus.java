package bus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class RemoveBus {
	public RemoveBus() {
	JFrame frame=new JFrame("REMOVE BUS");
	 frame.setSize(400,500);
	 
	 JLabel regnum=new JLabel("BUS REGISTER NUMBER");
	 regnum.setBounds(23,100,150,30);
	 frame.add(regnum);
	 
	 JTextField bus_textField=new JTextField();
	 bus_textField.setBounds(180,100,100,30);
	 frame.add(bus_textField);
	 JButton remove=new JButton("REMOVE");
	 remove.setBounds(30,200,100,30);
	 
	 frame.add(remove);

	 remove.addActionListener(new ActionListener()
	 {  
     public void actionPerformed(ActionEvent e)
     { 
    	 String registernumber=bus_textField.getText();
    	 if(registernumber.isEmpty()) {
    		 JOptionPane.showMessageDialog(frame," Enter the valid Detail"); 
    	 }
    	 
		try
		{  
			     Class.forName("oracle.jdbc.driver.OracleDriver");  
				   
				 Connection con=DriverManager.getConnection
				 ("jdbc:oracle:thin:@localhost:1521:xe","system","kalajayapal");  
				   
				 PreparedStatement stmt=con.prepareStatement("delete from busdetails where bus_regnum=(?)");  
				 
				 int regnum=Integer.parseInt(bus_textField.getText());
				 stmt.setInt(1,regnum); 
				 
				 int i=stmt.executeUpdate();  
				 JOptionPane.showMessageDialog(frame,i+"record deleted"); 
				 System.out.println(i+" record deleted");    
				 con.close();  
				   
		} 
		catch(Exception e1)
		{ 
	      System.out.println(e1);  
				   
		}}      
	     });
	 
	 JButton goBack=new JButton("BACK");
		goBack.setBounds(180,200,100,30);
		goBack.addActionListener(new ActionListener()
		{  
			 public void actionPerformed(ActionEvent e)
			 {  
				 AdminHomePage goback1=new AdminHomePage();
				 frame.dispose();       
		     }  
		});  
		 frame.add(goBack);
	 	
	  

		 frame.setLayout(null);
		 frame.setVisible(true);
}
}
				 

