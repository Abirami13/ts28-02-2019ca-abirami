package bus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import crudoperation.MainFrame;

public class ModifyBusDetails {
	public ModifyBusDetails(){

		JFrame frame=new JFrame("Modify Bus Details");
		 frame.setSize(400,500);
		 
		 JLabel busname=new JLabel("BUS NAME");
		 busname.setBounds(23,50,100,30);
		 frame.add(busname);
		  
		 JTextField nameField=new JTextField();
		 nameField.setBounds(150,50,100,30);
		 frame.add(nameField);
		 
		 JLabel regnum=new JLabel("BUS NUMBER");
		 regnum.setBounds(23,100,100,30);
		 frame.add(regnum);
		 
		 JTextField bus_textField=new JTextField();
		 bus_textField.setBounds(150,100,100,30);
		 frame.add(bus_textField);
		  
		 JLabel startpoint=new JLabel("SOURCE");
		 startpoint.setBounds(23,150,100,30);
		 frame.add(startpoint);
		 
		 JTextField city_nameField=new JTextField();
		 city_nameField.setBounds(150,150,100,30);
		 frame.add(city_nameField);
		 
		 JLabel starttime=new JLabel("START TIME");
		 starttime.setBounds(23,190,100,30);
		 frame.add(starttime);
		 
		 JTextField time_idField=new JTextField();
		 time_idField.setBounds(150,190,100,30);
		 frame.add(time_idField);
		 
		 JLabel endpoint=new JLabel("DESTINATION");
		 endpoint.setBounds(23,240,100,30);
		 frame.add(endpoint);
		 
		 JTextField city_nameField2=new JTextField();
		 city_nameField2.setBounds(150,240,100,30);
		 frame.add(city_nameField2);
		
		 JLabel droptime=new JLabel("DROP TIME");
		 droptime.setBounds(23,290,100,30);
		 frame.add(droptime);
		 
		 JTextField time_idField2=new JTextField();
		 time_idField2.setBounds(150,290,100,30);
		 frame.add(time_idField2);
		 
		 JLabel busfare=new JLabel("BUS FARE");
		 busfare.setBounds(23,340,100,30);
		 frame.add(busfare);
		 
		 JTextField busfarefield=new JTextField();
		 busfarefield.setBounds(150,340,100,30);
		 frame.add(busfarefield);
		
		 
		 
		 
		 JButton add=new JButton("UPDATE");
		 add.setBounds(30,390,100,30);
		 
		 	 frame.add(add);
		
		  add.addActionListener(new ActionListener()
		 {  
	     public void actionPerformed(ActionEvent e)
	     {
	    	 String busname=nameField.getText();
	    	 String busnumber=bus_textField.getText();
	    	 String source=city_nameField.getText();
	    	 String starttime=time_idField.getText();
	    	 String destination=city_nameField2.getText();
	    	 String droptime=time_idField2.getText();
	    	 String busfare=busfarefield.getText();
	    	 
	    	 if(busname.isEmpty()||busnumber.isEmpty()||source.isEmpty()||starttime.isEmpty()||destination.isEmpty()||droptime.isEmpty()||busfare.isEmpty()) 
	    	 {
	    		 JOptionPane.showMessageDialog(frame," Enter All The Details ");
	    			
	    	 }
			try
			{  
				 
				     Class.forName("oracle.jdbc.driver.OracleDriver");  
					   
					 Connection con=DriverManager.getConnection
					 ("jdbc:oracle:thin:@localhost:1521:xe","system","kalajayapal");  
					   
					 PreparedStatement stmt=con.prepareStatement("update busdetails set busname='"+busname+"',busfare='"+busfare+"',bus_startingpoint='"+source+"',bus_starttime='"+starttime+"',bus_droppoint='"+destination+"',bus_droptime="+droptime+" where bus_regnum="+busnumber+"");  
					 				     
					 int i=stmt.executeUpdate();  
					 JOptionPane.showMessageDialog(frame,i+"record updated"); 
					 System.out.println(i+" record updated");    
					 con.close();  
					   
			} 
			catch(Exception e1)
			{ 
		      System.out.println(e1);  
					   
			}}      
		     });
		  JButton goBack=new JButton("BACK");
			goBack.setBounds(190,390,100,30);
			goBack.addActionListener(new ActionListener()
			{  
				 public void actionPerformed(ActionEvent e)
				 {  
					 AdminHomePage goback1=new AdminHomePage();
					 frame.dispose();       
			     }  
			});  
			 frame.add(goBack);
			 frame.setLayout(null);
			 frame.setVisible(true);
	}	
}