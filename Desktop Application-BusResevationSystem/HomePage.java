package bus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

public class HomePage {
public	HomePage()  {
		JFrame f=new JFrame (" HOME PAGE");
        f.setSize(600, 400); 
        
        JButton admin=new JButton("ADMIN");
 		admin.setBounds(200,100,100,25);
 		
 		JButton agent=new JButton("AGENT");
 		agent.setBounds(200,150,100,25);
 		
 		f.add(admin);
 		f.add(agent);
 		f.setLayout(null);
	    f.setVisible(true);				

	   		admin.addActionListener(new ActionListener()
			 {  
				 public void actionPerformed(ActionEvent e)
				 {  
					 AdminLogin view=new AdminLogin();
					 f.dispose();
			             
			     }  
			 });
	 		
	 		agent.addActionListener(new ActionListener()
			 {  
				 public void actionPerformed(ActionEvent e)
				 {  
					 AgentLogin view=new AgentLogin();
					 f.dispose();
			             
			     }  
			 });
	 		
        
}
public static void main(String[] args) {
	HomePage h=new HomePage();
}
}