package bus;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;


public class AdminHomePage {

	public AdminHomePage() {
		JFrame f= new JFrame("ADMIN HOME PAGE");
		JButton b=new JButton("ADD BUS ROUTE");  
	    b.setBounds(70,100,160,30);
	    JButton c=new JButton("REMOVE BUS");
	    c.setBounds(70,150,160,30);
	    JButton d=new JButton("MODIFY BUS DETAILS");
	    d.setBounds(70,200,160,30);
	    JButton view=new JButton("VIEW BUS DETAILS");
	    view.setBounds(70,250,160,30);
	    
	    JButton sign=new JButton("SIGN OUT");
	    sign.setBounds(70,300,160,30);
	    
	    
	    b.addActionListener(new ActionListener()
		 {  
			 public void actionPerformed(ActionEvent e)
			 {  
				 AddBusDetails AddBusRoute=new AddBusDetails();
				 f.dispose();
		             
		     }  
		 }); 
	    
	   
	    d.addActionListener(new ActionListener()
		 {  
			 public void actionPerformed(ActionEvent e)
			 {  
				 ModifyBusDetails Adddetails=new ModifyBusDetails();
				 f.dispose();
		             
		     }  
		 }); 
	    c.addActionListener(new ActionListener()
		 {  
			 public void actionPerformed(ActionEvent e)
			 {  
				 RemoveBus Add=new RemoveBus();
				 f.dispose();
		             
		     }  
		 }); 
	    
	    view.addActionListener(new ActionListener()
		 {  
			 public void actionPerformed(ActionEvent e)
			 {  
				 ViewBusDetails Add=new ViewBusDetails();
				 
		             
		     }  
		 }); 
	    
	    
	    sign.addActionListener(new ActionListener()
		 {  
			 public void actionPerformed(ActionEvent e)
			 {  
				 HomePage home=new HomePage();
				 f.dispose();
				 			 
		             
		     }  
		 }); 
	    
		
	    f.add(view);
	    f.add(b);
	    f.add(c);
	    f.add(d);
	    f.add(sign);
	    f.setSize(400,500);  
	    f.setLayout(null);  
	    f.setVisible(true);
	 
		
	     }
}
